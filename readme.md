# Run the following commands to setup and start the server
Extract the zip file and cd into `kanban-app` folder to set up and run the application.

## Creating a database
Run the following commands to create a database with some data (if not already created) `sqlite3 database.sqlite3`. Then run the following commands
```
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "role" (
	"id"	INTEGER,
	"name"	TEXT UNIQUE,
	"description"	,
	"TEXT"	,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "roles_users" (
	"id"	INTEGER,
	"user_id"	INTEGER,
	"role_id"	INTEGER,
	FOREIGN KEY("user_id") REFERENCES "user"("id"),
	FOREIGN KEY("role_id") REFERENCES "role"("id")
);
CREATE TABLE IF NOT EXISTS "user" (
	"id"	INTEGER,
	"username"	TEXT,
	"email"	TEXT UNIQUE,
	"password"	TEXT,
	"active"	INTEGER,
	"updated"	TEXT,
	"list_updated"	TEXT,
	"card_updated"	TEXT,
	"fs_uniquifier"	TEXT UNIQUE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "list" (
	"list_id"	INTEGER UNIQUE,
	"name"	TEXT,
	"description"	TEXT,
	"user_id"	INTEGER,
	"last_updated"	TEXT,
	FOREIGN KEY("user_id") REFERENCES "user"("id"),
	PRIMARY KEY("list_id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "card" (
	"card_id"	INTEGER UNIQUE,
	"name"	TEXT,
	"description"	TEXT,
	"user_id"	INTEGER,
	"list_id"	INTEGER,
	"completed"	TEXT,
	"deadline"	Text,
	"last_updated"	TEXT,
	PRIMARY KEY("card_id" AUTOINCREMENT),
	FOREIGN KEY("user_id") REFERENCES "user"("id"),
	FOREIGN KEY("list_id") REFERENCES "list"("list_id")
);
COMMIT;

```

## Creating the virtual environment (if not already created)

```bash
apt install python3.8-venv
python3.8 -m venv .env

```

## Launching the virtual environment
```bash
source .env/bin/activate

```
## Installing the required packeges
```bash
pip install --upgrade pip
pip install -r requirements.txt
```

## Starting the server
```bash
python main.py
```

## Frontend
To run the frontend on a different server, cd into `frontend` and read the `readme.md` file.
