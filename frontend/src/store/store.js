
var initial = {
    "lists": {},
    "cards": {},
    "auth_token": "",
    "user": { "name": "unset",
        "email": "None",
        },
    "updated": "2022-01-01 01:00:00.000000"
    }
if (localStorage.getItem("kanban") ){
   initial = JSON.parse(localStorage.getItem("kanban"))
}

export const store = {
    list: {},
    kanban: initial,
    
    
    userData(auth_token) {
        localStorage.setItem("kanban", JSON.stringify(initial));
        this.kanban.auth_token=auth_token.authentication_token;
        localStorage.setItem("kanban", JSON.stringify(this.kanban));
    },
    resetData() {
        console.log("LOGINGOUT");
        this.kanban.auth_token="";
        this.kanban.updated="2022-01-01 01:00:00.000000";
        this.kanban.lists={};
        this.kanban.cards={};
        this.kanban.user={};
        localStorage.setItem("kanban", JSON.stringify(initial));
    },
    UpdateLocalStorage(data) {
    this.kanban['updated']=data['updated'];
    for (let l in data['lists']) {
        let lst = data['lists'][l];
        if (! (l in this.kanban['lists']) ) {
            this.kanban['lists'][l]=lst;
            this.kanban['lists'][l]['cards']={};
        }
        else {
            this.kanban['lists'][l]['name']=lst['name'];
            this.kanban['lists'][l]['description']=lst['description'];
        }
    }
    for (let c in data['cards']) {
        let card=data['cards'][c];
        if (! (c in this.kanban['cards'] ) ){
            this.kanban['cards'][c]=card;
            this.kanban['lists'][card.list_id]['cards'][c]=card;
        }
        else {
            let oldlist = this.kanban['cards'][c].list_id;
            if (oldlist != card.list_id) {
                delete this.kanban['lists'][oldlist]['cards'][c];
            }
            this.kanban['cards'][c]=card;
        }
        this.kanban['lists'][card.list_id]['cards'][c]=card;
    }

    if ('user' in data) {
        this.kanban['user']=data['user'];
    }
    
    if ('card_ids' in data) {
        for (let c in this.kanban['cards'] ) {
            if (data['card_ids'].indexOf(Number(c))==-1 ) {
                let card=this.kanban['cards'][c];
                delete this.kanban['lists'][card.list_id]['cards'][c];
                delete this.kanban['cards'][c];
            }
        }
    }
    
    if ('list_ids' in data) {
        for (let l in this.kanban['lists']) {
            if (data['list_ids'].indexOf(Number(l)) == -1) {
                delete this.kanban['lists'][l];
            }
        }
    }
    localStorage.setItem("kanban", JSON.stringify(this.kanban));
}




};
