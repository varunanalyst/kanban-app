import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
//import createWebHistory from 'vue-router'
//import createRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
  path: '/list/',
  name: 'list',
  component: () => import('../views/ListPage.vue')
  },
  {
  path: '/list/:id',
  name: 'single-list',
  component: () => import('../views/SingleList.vue')
  },
]

const router = new VueRouter({
  routes
})

/*const router = new VueRouter({
  mode: 'history',
  //history: createWebHashHistory(),
  base: process.env.BASE_URL,
  routes
})*/

export default router
