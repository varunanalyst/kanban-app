import {store} from "../store/store.js";

import exportFromJSON from 'export-from-json';

const exportType =  exportFromJSON.types.csv;


function exportCards(orderedcards) {
    let result=[]
    for (let i in orderedcards) {
        let cardkey=orderedcards[i];
        let card=this.kanban.cards[FindId(cardkey)]
        result.push(card);
    }
    let fileName = 'cards';
    exportFromJSON({ data:result, fileName, exportType })
}

function exportLists(lists) {
    let result=[]
    for (let i in lists) {
        let list=lists[i];
        let stats=getStats(list);
        let col = {
            id: list.id,
            name: list["name"],
            description: list["description"],
            completed: stats.completed,
            passed_deadline: stats.passed_deadline,
            pending: stats.pending,
            
        }
        result.push(col)
    }
    let fileName = 'lists';
    exportFromJSON({ data:result, fileName, exportType })
}

function FindKey(card) {
    return card.deadline+ ( "0".repeat(10-String(card.id).length) ) +card.id;
}

function FindId(cardkey) {
    for (let i=10;i<20;i++) {
        if (cardkey[i]!="0") {
            return cardkey.slice(i);
        }
    }
    return "0";
}

function getCardOrder(cards) {
    //return a dictionary with the keys as the deadline plus id, and the values as the deadline
    let result = [];
    for (let c in cards) {
        result.push( FindKey(cards[c]) )
    }
    result.sort();
    return result;
}

function getPendingCardOrder(cards) {
    let today = new Date();
    //return a dictionary with the keys as the deadline plus id, and the values as the deadline
    let result = [];
    for (let c in cards) {
        let card=cards[c];
        let d1 = new Date(card.deadline+" 23:59:59");
        if ( (d1 > today ) && (card.completed == "0" ) ) {
            result.push( FindKey(card) )
        }
    }
    result.sort();
    return result;
}

async function UpdateAll(response) {
    let data = await response;
    console.log(data);
    if (!data['updated']) {
        alert("Unable to update data! "+data["message"]);
        return
    }

    store.UpdateLocalStorage(data);
}

async function UpdateUser(response) {
    let data = await response;
    console.log(data);
    if (!data.response.user) {
        alert("Unable to login! Please DELETE SESSION COOKIES if you are having trouble. "+data.response.errors);
        return
    }

    store.userData(data.response.user);
    this.UpdateAll(this.getUpdates(this.kanban["updated"]))
}

function LogoutUser() {
    store.resetData()
}



function getStats(l) {
    /*
    Statistics of the following based on the list id:
        * completed tasks
        * crossed deadline
        * pending
        * completed frequency
        * cards belonging to list
    */

    let result = {
        'completed': 0,
        'passed_deadline': 0,
        'pending': 0,
        'completed_freq': {},
        'cards': {}
        }
    let today = new Date();
    for (let c in l.cards) {
        let card=l.cards[c];
        let d1 = new Date(card.deadline+" 23:59:59");
        //let lid = l.list_id;
        // if completed
        let completed=card.completed;
        if (completed != "0" ) {
            result.completed+=1;
            if (completed in result.completed_freq) {
                result.completed_freq[completed]+=1;
                }
            else {
                result.completed_freq[completed]=1;
            }
        }
        // incomplete, crossed deadline
        else if (d1 <= today ) {
            result.passed_deadline+=1;
        }
        // Pending
        else {
            result.pending+=1;
        }
    }
    return result
}


async function userLogin(email, password) {
    console.log("Logingin");
    try {
    const url="http://127.0.0.1:8080/login?include_auth_token";
    const response = await fetch(url, {
        mode: 'cors',
        method: 'POST',
        credentials: "same-origin",
        //credentials: "include",
        headers: {
            'Content-Type': 'application/json',
             },
        body: JSON.stringify({"email": email, "password": password})
    })
        let data =  await response.json();
        console.log(data)
        return data;
    } catch (err) {
        let result = {};
        //data.response.errors
        result["message"] = String(err)
        result["response"]= {"errors": err}
        result["updated"] = false;
        return result;
    }
}

async function userRegister(email, password) {
    console.log("Registering");
    try {
    const url="http://127.0.0.1:8080/register?include_auth_token";
    const response = await fetch(url, {
        mode: 'cors',
        method: 'POST',
        credentials: "same-origin",
        //credentials: "include",
        headers: {
            'Content-Type': 'application/json',
             },
        body: JSON.stringify({"email": email, "password": password})
    })
        let data =  await response.json();
        console.log(data)
        return data;
    } catch (err) {
        let result = {};
        //data.response.errors
        result["message"] = String(err)
        result["response"]= {"errors": err}
        result["updated"] = false;
        return result;
    }
}


async function getUpdates(updated="2022-01-01 01:00:00.000000") {
    console.log("UPDATING");
    try {
    const url="http://127.0.0.1:8080/api/update";
    const response = await fetch(url, {
        mode: 'cors',
        method: 'PUT',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        
        body: JSON.stringify({"updated": updated})
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

async function createList(name, description, updated) {
    console.log("CREATING LIST");
    try {
    const url="http://127.0.0.1:8080/api/list/new";
    const response = await fetch(url, {
        method: 'PUT',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        body: JSON.stringify({"name": name,
            "description": description,
            "updated": updated
            })
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

async function UpdateList(id, name, description, updated) {
    try {
    const url="http://127.0.0.1:8080/api/list/"+id;
    const response = await fetch(url, {
        method: 'PUT',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        body: JSON.stringify({"name": name,
            "description": description,
            "updated": updated
            })
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

async function createCard(name, description, list_id, completed, deadline, updated) {
    console.log("CREATING CARD");
    try {
    const url="http://127.0.0.1:8080/api/card/new";
    const response = await fetch(url, {
        method: 'PUT',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        body: JSON.stringify({
            "name": name,
            "description": description,
            "list_id": list_id,
            "completed": completed,
            "deadline": deadline,
            "updated": updated
            })
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

async function updateCard(id, name, description, list_id, completed, deadline, updated) {
    console.log("UPDATING CARD");
    try {
    const url="http://127.0.0.1:8080/api/card/"+id;
    //console.log(url);
    const response = await fetch(url, {
        method: 'PUT',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        body: JSON.stringify({
            "name": name,
            "description": description,
            "list_id": list_id,
            "completed": completed,
            "deadline": deadline,
            "updated": updated
            })
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

async function deleteCard(id, updated) {
    console.log("UPDATING CARD");
    try {
    const url="http://127.0.0.1:8080/api/card/"+id;
    //console.log(url);
    const response = await fetch(url, {
        method: 'DELETE',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        body: JSON.stringify({
            "updated": updated
            })
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

async function deleteList(id, new_list, updated) {
    console.log("Deleting List");
    try {
    const url="http://127.0.0.1:8080/api/list/"+id;
    //console.log(url);
    const response = await fetch(url, {
        method: 'DELETE',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            "authentication_token": this.kanban.auth_token,
             },
        body: JSON.stringify({
            "updated": updated,
            "new_list": new_list,
            })
    })
        let data =  await response.json();
        return data;
    } catch (err) {
        let result = {};
        result["message"] = String(err)
        result["updated"] = false;
        return result;
    }
}

export const UpdateData = {
    data () {
        return {
        //kanban,
        //liststats
        }
    },
    methods: {
        UpdateAll,
        //UpdateLocalStorage,
        getUpdates,
        FindKey,
        FindId,
        getCardOrder,
        getPendingCardOrder,
        getStats,
        
        createList,
        UpdateList,
        
        createCard,
        updateCard,
        
        deleteCard,
        deleteList,
        
        userLogin,
        UpdateUser,
        LogoutUser,
        userRegister,
        
        exportCards,
        exportLists
    },
}
