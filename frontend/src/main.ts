import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

//import VueComp from '@vue/composition-api';

Vue.config.productionTip = false

//Vue.use(VueComp);

new Vue({
  router,
  store,
  render: h => h(App),
  components: { App }
}).$mount('#app')

