# Run the following commands to setup and start the server
cd into the `frontend` folder to set up and run the application. 

## Installing NPM and Vue CLI (if not already installed)
NPM needs to be installed, and Vue CLI needs to be installed globally

```bash
```bash
npm audit fix
```

## Installing the required packages
run the following command to create the `node_modules`.

```bash
npm audit fix
```

## running the server
```bash
vue serve

```
## building the server
```bash
vue build
```

## Backend
If you run the `vue build` command, the app will be compiled to be run directly from the backend server.
