import os
main_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "../db_directory")

class Config():
    SQLALCHEMY_DATABASE_URI = "sqlite:///" +os.path.join(main_dir, "database.sqlite3")
    DEBUG=True
    SECRET_KEY="acarceeuc"
    SECURITY_TOKEN_AUTHENTICATION_HEADER = "Authentication-Token"
    SECURITY_PASSWORD_HASH="bcrypt"
    SECURITY_PASSWORD_SALT= "cnscl3aajic08o"
    SECURITY_REGISTERABLE = True
    SECURITY_CONFIRMABLE = False
    #SECURITY_UNAUTHORIZED_VIEW=None
    WTF_CSRF_ENABLED=False
    SECURITY_SEND_REGISTER_EMAIL=False
    SESSION_COOKIE_SAMESITE="Strict"
    LOGIN_DISABLED=False
    TESTING=False
    
