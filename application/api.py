from flask_restful import Resource, Api
from flask_restful import fields, marshal
from datetime import datetime
import json
from flask import request
from flask_restful import reqparse
from application.models import *

from flask import current_app as app
from flask import abort, make_response, session
from flask_security import auth_required, login_required, roles_accepted, roles_required, auth_token_required, current_user

from flask_cors import CORS, cross_origin

class Update(Resource):
    def get(self):
        print(request)
        if not current_user.is_authenticated:
            return {"message": "authentication required" }, 401
        else:
            if not bool(current_user.updated):
                current_user.updated="2022-01-01 01:01:50.000000"
                current_user.list_updated="2022-01-01 01:01:50.000000"
                current_user.card_updated="2022-01-01 01:01:50.000000"
                db.session.commit()
                
            user=current_user
        print(user.email)
        updated = request.cookies.get("updated")
        if not updated:
            updated="2022-01-01 01:01:00.000000"
        return getAll(current_user, updated)
        
    def put(self):
        print(current_user.is_authenticated)
        if not current_user.is_authenticated:
            return {"message": "authentication required" }, 401
        else:
            if not bool(current_user.updated):
                current_user.updated="2022-01-01 01:01:50.000000"
                current_user.list_updated="2022-01-01 01:01:50.000000"
                current_user.card_updated="2022-01-01 01:01:50.000000"
                db.session.commit()
            user=current_user
        print(user.email)
        data = json.loads(request.data)
        if (set(data.keys()) != set({'updated'}) ):
            return {'message': 'incorrect format!' }
        return getAll(user, data['updated'])


parser = reqparse.RequestParser()

class ManageList(Resource):
    def put(self, list_id):
        if not current_user.is_authenticated:
            return {"message": "authentication required" }, 401
        data = json.loads(request.data)
        if (set(data.keys() ) != set({"name", "description", "updated"}) ):
            return {"message": "incorrect format!" }
        updated=data['updated']
        if list_id=="new":
            print("NEW LIST")
            l = List(
                name=data["name"],
                description=data["description"],
                user_id=current_user.id,
                last_updated=str(datetime.now())
            )
            db.session.add(l)
            current_user.list_updated=l.last_updated
        else:
            l=List.query.filter( (List.user_id==current_user.id) & (List.list_id==list_id) ).first()
            if not bool(l):
                return {"message": "list not found!" }
            l.name=data["name"]
            l.description=data["description"]
            l.user_id=current_user.id
            l.last_updated=str(datetime.now())

        db.session.commit()
        resp = make_response(getAll(current_user, updated, message="list updated successfully") )
        resp.set_cookie('updated', str(datetime.now() ), samesite="Lax")
        return resp
    
    def delete(self, list_id):
        if not current_user.is_authenticated:
            return {"message": "authentication required" }, 401
        data = json.loads(request.data)
        if (set(data.keys()) != set({'updated', "new_list"}) ):
            print(set(data.keys(), set({"updated", "new_list"})) )
            return {'message': 'incorrect format!' }
        updated=data['updated']
        new_list=data['new_list']
        l=List.query.filter((List.user_id==current_user.id) & (List.list_id==list_id)).first()
        nl = List.query.filter( (List.user_id==current_user.id) & (List.list_id==new_list)).first()
        if not ( bool(l) and bool(nl) ):
            return {"message": "given list not found!" }, 404
        
        cards=Card.query.filter_by(list_id=list_id).all()
        if (nl.list_id==l.list_id):
            for c in cards:
                db.session.delete(c)
        else:
            for c in cards:
                c.list_id=new_list
                c.last_updated=str(datetime.now())
        
        db.session.commit()
        db.session.delete(l)
        current_user.list_updated=str(datetime.now())
        current_user.card_updated=str(datetime.now())
        db.session.commit()
        return getAll(current_user, updated, message="List deleted successfully") 


class ManageCard(Resource):
    #@cross_origin(supports_credentials=True)
    def put(self, card_id):
        if not current_user.is_authenticated:
            return {"message": "authentication required" }, 401
        data = json.loads(request.data)
        if set(data.keys()) != set({"name", "description", "list_id", "completed", "deadline", "updated"}):
            return {"message": "Incorrect format!" }
        updated=data['updated']
        l=List.query.filter((List.user_id==current_user.id) & (List.list_id==data["list_id"])).first()
        if not bool(l):
            return {"message": "given list not found!" }, 404
        if card_id=="new":
            print("NEW CARD")
            c = Card(
                name=data["name"],
                description=data["description"],
                user_id=current_user.id,
                list_id=l.list_id,
                completed=data["completed"],
                deadline=data["deadline"],
                last_updated=str(datetime.now())
            )
            db.session.add(c)
            message="card updated successfully!"
            current_user.card_updated=c.last_updated
        else:
            c=Card.query.filter( (Card.user_id==current_user.id) & (Card.card_id==card_id) ).first()
            if not bool(c):
                return {"message": "card not found!" }, 404
            c.name=data["name"]
            c.description=data["description"]
            c.user_id=current_user.id
            c.list_id=l.list_id
            c.completed=data["completed"]
            c.deadline=data["deadline"]
            c.last_updated=str(datetime.now())
            message="card updated successfully!"

        if data["completed"] == "0":
            c.completed = "0"
        else:
            c.completed= str(datetime.now())[:10]
        db.session.commit()
        return getAll(current_user, updated, message=message) 
        
    def delete(self, card_id):
        if not current_user.is_authenticated:
            return {"message": "authentication required" }, 401
        data = json.loads(request.data)
        if (set(data.keys()) != set({'updated'}) ):
            return {'message': 'incorrect format!' }
        updated=data['updated']
        c=Card.query.filter((Card.user_id==current_user.id) & (Card.card_id==card_id)).first()
        if not bool(c):
            return {"message": "card not found!" }, 404        
        db.session.delete(c)
        current_user.card_updated=str(datetime.now())
        db.session.commit()
        return getAll(current_user, updated, message="Card deleted successfully")
        
def getDetails(user):
    return {"id": user.id, "username": user.username, "email": user.email }

def getLists(user):
    r = []
    l = List.query.filter_by(user_id=user.id).all()
    for i in l:
        r.append(i.list_id)
    return r

def getCards(user):
    r = []
    c = Card.query.filter_by(user_id=user.id).all()
    for i in c:
        r.append(i.card_id)
    return r

def getAll(user, updated="2022-01-01 01:00:00.000000", message="update", address=False):
    user_id=int(user.id)
    result = {}
    print(user.updated, updated)
    if user.updated > updated:
        result["user"] = getDetails(user)
    if user.list_updated > updated:
        result["list_ids"] = getLists(user)
    if user.card_updated > updated:
        result["card_ids"] = getCards(user)
    
    lists=List.query.filter( (List.user_id==user.id) & (List.last_updated > updated) ).all()
    l={}
    for i in lists:
        l[i.list_id] = {"id": i.list_id,
            "name": i.name,
            "description": i.description
        }
    result["lists"]=l
    
    cards=Card.query.filter( (Card.user_id==user.id) & (Card.last_updated > updated) ).all()
    c={}
    for i in cards:
        c[i.card_id] = {"id": i.card_id,
            "name": i.name,
            "description": i.description,
            "list_id": i.list_id,
            "completed": i.completed,
            "deadline": i.deadline
        }
        
    result["cards"]=c
    result["updated"]=str(datetime.now())
    result["message"]=message
    
    return result

