from sqlalchemy.ext.declarative import declarative_base

from flask_sqlalchemy import SQLAlchemy
from flask_security import UserMixin, RoleMixin

#Base=declarative_base()
db=SQLAlchemy()

roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id') ),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id') )
    )

class User(db.Model, UserMixin):
    __tablename__='user'
    id=db.Column(db.Integer, autoincrement=True, primary_key=True)
    username=db.Column(db.String)
    email=db.Column(db.String, unique=True)
    password=db.Column(db.String)
    active=db.Column(db.Boolean())
    updated=db.Column(db.String)
    list_updated=db.Column(db.String)
    card_updated=db.Column(db.String)
    fs_uniquifier=db.Column(db.String, unique=True, nullable=False)
    roles=db.relationship('Role', secondary=roles_users, backref=db.backref('users', lazy='dynamic') )

class Role(db.Model, RoleMixin):
    __tablename__ = 'role'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)

class List(db.Model):
    __tablename__='list'
    list_id=db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    name=db.Column(db.String)
    description=db.Column(db.String)
    user_id=db.Column(db.Integer, db.ForeignKey("user.id") )
    last_updated=db.Column(db.String)

class Card(db.Model):
    __tablename__="card"
    card_id=db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    name=db.Column(db.String)
    description=db.Column(db.String)
    user_id=db.Column(db.Integer, db.ForeignKey('user.id') )
    list_id=db.Column(db.Integer, db.ForeignKey('list.list_id') )
    last_updated=db.Column(db.Text)
    deadline=db.Column(db.String)
    completed=db.Column(db.String)

