import os
from datetime import datetime
from flask import Flask, redirect, request, make_response
from flask import url_for
from flask_restful import Resource, Api
from flask_cors import CORS

from flask_sqlalchemy import SQLAlchemy

from flask_security import Security, SQLAlchemySessionUserDatastore, SQLAlchemyUserDatastore, utils

from application import config
from application.config import Config


from application.models import db, User, Role


app = Flask(__name__, static_url_path='/app', static_folder='frontend/dist')
app.config.from_object(Config)
current_dir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" +os.path.join(current_dir, "database.sqlite3")

app.app_context().push()

db.init_app(app)

api = Api(app)

cors = CORS(app, resources={
    r"*": {"origins": "*", }},
    expose_headers="*",
    allow_headers="*",
    support_credentials=True,
     )


# Setting up Flask Security
user_datastore=SQLAlchemySessionUserDatastore(db.session, User, Role)
security=Security(app, user_datastore)


from application.api import *
api.add_resource(Update, "/api/update")
api.add_resource(ManageList, "/api/list/<list_id>")
api.add_resource(ManageCard, "/api/card/<card_id>")


# CONTROLLERS
@app.route("/")
def home():
    return redirect('/app/')

@app.route("/app/")
def Index():
   return redirect('/app/index.html')

@app.route('/favicon.ico')
def favicon():
    return send_file('favicon.ico')

if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        debug=True,
        port=8080
	) 
